# Python Lorem Pixel
Author: Tim Santor <tsantor@xstudios.agency>

# Overview
Grab any number of images from Lorem Pixel easily.

# Usage

Simple:

    lorem-pixel 512x512 10

Advanced:

    lorem-pixel 512x512 10 -c city -o downloads

**NOTE**: For all available options, run `lorem-pixel -h`.

# Version History

- **0.0.1** - Initial release
- **0.0.2** - Added missing dependency to setup.py
- **1.0.0** - Python 2 and 3 compatible

# Issues

If you experience any issues, please create an [issue](https://bitbucket.org/tsantor/python-lorem-pixel/issues) on Bitbucket.
